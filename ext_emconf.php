<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "extdeveval".
 *
 * Auto generated 16-04-2015 15:26
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Extension Development Evaluator - 6.2 VERSION',
	'description' => 'A backend development module that offers features to help develop and evaluate various features of extensions under development',
	'category' => 'module',
	'version' => '6.2.0',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'clearcacheonload' => 0,
	'author' => 'TYPO3 v4 Core Team, DKM',
	'author_email' => '',
	'author_company' => '',
	'constraints' => 
	array (
		'depends' => 
		array (
			'php' => '5.2.0-0.0.0',
			'typo3' => '6.2.0-7.0.99',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
);

