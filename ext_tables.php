<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

if (TYPO3_MODE=='BE')	{
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addModule('tools', 'txextdevevalM1', '',\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'mod1/');

	// register top module
	$GLOBALS['TYPO3_CONF_VARS']['typo3/backend.php']['additionalBackendItems'][] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('extdeveval') . 'registerToolbarItem.php';
}
?>